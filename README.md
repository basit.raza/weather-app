# Weather app

A simple web application that consume [Meta Weather](metaweather.com) api to query the location weather.

## Meta Weather api wrapper setup

I am assuming you have nginx and php setup and webroot is set to `/var/www/html`.

```bash
cp server/weather.php /var/www/html
chown www-data /var/www/html/weather.php
```

If your webserver is bind to hostname other then `localhost`, you need to change the hostname in `src/api/weather.js`

```javascript
const baseURL = 'http://<your-host-name>/weather.php'
```

## Frontend setup

```bash
npm install
```

### Compiles and hot-reloads for development

```bash
npm run serve
```

### Compiles and minifies for production

```bash
npm run build
```

### Lints and fixes files

```bash
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### Demo

see [Screenshots](screenshots/README.md)